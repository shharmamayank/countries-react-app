import React from 'react'

export default function SubRegion(props) {
    const { data } = props

    return (

        <option value={data}>{data}</option>

    )
} 
