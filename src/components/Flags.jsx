import React from 'react'
import { useState } from 'react'
import countries from "./countries.json";
import Card from "./Card"
import "./Flags.css"
import SubRegion from './SubRegion';
export default function Flags() {

    const allData = countries.map(data => {
        return <Card data={data} />
    })
    const [value, setValue] = useState(allData)
    function handleSearch(SearchBarData) {
        let searchData = SearchBarData.target.value
        const filteredByCountry = countries.filter(data => {
            return data.name.common.toString().includes(searchData)
        }).map(data => {
            return <Card data={data} />
        })
        setValue(filteredByCountry)
    }
    function handleFilterButton(event) {
        const countryValue = event.target.value
        const filteredByRegion = countries.filter(data => {
            return data.region.toString().includes(countryValue)
        }).map(data => { 
            return <Card data={data} />
        })
        setValue(filteredByRegion)
    }
    const allRegion = countries.reduce((prev, curr) => {
        if (!prev.includes(curr.region)) {
            prev.push(curr.region)
        }
        return prev
    }, [])
    const totalRegion = allRegion.map(data => {
        return <SubRegion data={data} />
    })
    const allSubRegion = countries.reduce((prev, curr) => {
        if (!prev.includes(curr.subregion)) {
            prev.push(curr.subregion)
        }
        return prev
    }, [])
    const totalSubregion = allSubRegion.map(data => {
        return <SubRegion data={data} />
    })
    return (
        <>
            <div className="flag-main-div">
                <div>
                    <input onChange={handleSearch} type='search' placeholder="Search" />
                    <button type="send">Search</button>
                </div>
                {/* <div class="input-group mb-9 w-50"> */}
                <div>
                    <select onClick={handleFilterButton}>
                        <option value="">filter by region</option>
                        {totalRegion}
                    </select>
                </div>

                <div>
                    <div className="input-group mb-9 w-50">
                        <button className="btn btn-outline-secondary dropdown-toggle" type="button" data-bs-toggle="dropdown" aria-expanded="false">Dropdown</button>
                        <ul className="dropdown-menu">
                            {/* <li><a className="dropdown-item" href="#">{totalSubregion}</a></li> */}
                            <div className="dropdown-item" href="#">{totalSubregion}</div>
                        </ul>
                        {/* <select>
                        <option>filter by SubRegion</option>
                        {totalSubregion}
                    </select> */}
                    </div>

                </div>
            </div >

            <div className="flag-data">{value}</div>
        </>
    )
}
