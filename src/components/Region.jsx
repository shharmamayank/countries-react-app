import React from 'react'

export default function Region(props) {
    const { data } = props
    
    return (
        <div>
            <option value={props.data}>{data}</option>
        </div>
    )
}
