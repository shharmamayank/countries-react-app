import React from 'react'
import "./Card.css"

export default function card(props) {
    const { png } = props.data.flags
    const { region, capital } = props.data
    const { official } = props.data.name
    return (
        <div className="check-dis">
            <img className='img-div' src={png} alt="" />
            <p>country:{official}</p>
            <p>capital:{capital}</p>
            <p>region:{region}</p>
        </div>
    )
}
