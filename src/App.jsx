import { useState } from 'react'
import './App.css'
import NavBar from './components/NavBar'
import data from "./components/countries.json"
import Flags from './components/Flags'
function App() {
  return (
    <>
      <NavBar />
      <Flags />
      
    </>
  )
}

export default App
